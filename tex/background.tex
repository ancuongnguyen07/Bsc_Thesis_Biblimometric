Before we go to the implementation section, we need to be familiar with some
core concepts: GOTO ranking, CSRankings, dblp, and Version Control with Git.

GOTO ranking is a metric-based ranking methodology which CSRankings bases on.
In addition, CSRankings assesses higher education institutions based on
the number of academic papers published at top computer science conferences.
To access the database of bibliographic information on these conferences,
CSRankings uses dblp, an open bibliography provider. Besides,
CSRankings is an open source project which we can contribute to through
a specific state capture mechanism. In this case, Git is the version
control system that is used by CSRankings to manage snapshots or states
along the timeline of it.

\section{GOTO ranking}

% More ranking problems needed
Higher education institutions are mostly ranked
by for-profit organizations~\cite{GOTO_ranking}. Hence, the business-first goal of
those ranking organizations is inevitable. For-profit ranking enterprises
usually tune their methodology in order to make variation in the rankings. The
movement in the rankings is needed for making the business viable~\cite{harmful_ranking}.
Furthermore, the methodology
of certain rankings is highly limited. These rankings, reputation-based rankings, depend
solely or partly on the reputation surveys which
have several limitations. The first disadvantage is that the reputation is prone to change
slowly, because it can take years for the reputation of a university to
be updated when a department improves~\cite{GOTO_ranking,reputation_change}.
In addition, the assessment, in which department chairs and graduate directors
are asked to evaluate each program on the scale of 1 to 5, is subjective~\cite{harmful_ranking}.
The scores are made based only on the personal opinion without objective data,
not showing any indication of whether productivity or reputation is measured~\cite{Shin2011_reputation_ranking}.
In addition, reputation-based rankings have a considerable influence on assessments responded by faculty members
about institutions' reputation, which is known as the anchoring theory: current judgements may be
influenced by prior evaluations that were represented in rankings, resulting in duplicated
and reinforced evaluations~\cite{anchor_effect,rankings_and_reactivity}. According to~\cite{anchor_effect},
the anchoring theory states that ``people use the starting value to inform their judgements,
and then they adjust (insufficiently) this value when making their final judgement,
even when the starting value is entirely random''. As the following
ranking anchors the observers' assessments of each higher education institution, the
anchoring theory has a substantial impact on shaping perceptions based on the initial
rankings as well as impacting current perceptions. Along with the
anchoring theory, the echo effect may have impact on consecutive rankings: universities affect the judgements
that are successively used to produce following rankings by using their ability to simplify,
and their effective communication networks~\cite{inter_reputation_based_ranking}.
Therefore, the subsequent assessments of the reputation of universities can be misled
as the initial reviews can be reinforced and widely broadcasted by the media~\cite{echo_effect}.
A circle known as ``reputation-ranking-reputation'' results when the anchor effect is
repeated dynamically and the echo effect is introduced~\cite{inter_reputation_based_ranking}.
In other words, it is possible that the position of institutions in many reputation-based
rankings does not illustrate the current quality but their prior reputation that has been gained since a
long time ago. Furthermore, the halo effect may have a biased influence on the reputation
surveys as well. The halo effect, or the halo error, is a cognitive bias in which
individuals generate opinions about a quality or attribute of a product depending
on their propensity toward another aspect~\cite{halo_effect}.
For instance, rather than utilizing ranking factors that
are closely related to the performance in terms of teaching, research, or the
so-called third mission including promoting innovation, entrepreneurship,
knowledge transfer and social commitment, several rankings are based on metrics
that are connected to the size and age of institutions~\cite{x_factor_ranking}.
As a result, the halo effect of renowned universities' parent institutions
raises the ranks of their graduate programs~\cite{harmful_ranking}.

To address aforementioned disadvantages of reputation-based rankings, a new model
of ranking factor should be introduced.
As mentioned in~\cite{GOTO_ranking}, the Computing Research
Association (CRA) has suggested developing new methods which are data-driven, based on
meaningful metrics, and at least meet the following criteria:

\begin{itemize}
    \item \textbf{G}ood data: have been cleaned and curated.
    \item \textbf{O}pen: data is available, regarding attributes measured, at least
    for verification.
    \item \textbf{T}ransparent: process and methodologies are entirely transparent.
    \item \textbf{O}bjective: based on measurable attributes.
\end{itemize}

The requirement of \textbf{G}ood data means that data should be accurate and reflect
correctly how research is disseminated in a particular scientific
community. For example, in the computer science area, conference publications
are the most influential and highly cited peer reviewed articles. However, in 2017,
\gls{usn_wr} conducted a ranking of all computer science departments over the world
based on journal publications, ignoring academic papers published at conferences.
As a result, the output ranking did not reflect precisely how research works were
propagated in the computer science community or how academics were rewarded or had
an influence~\cite{cra_usnews}. Although objective data was used in this case,
the ranking was still implausible due to the shortage of data coverage. In order to
address this issue, in 2019, \acrshort{usn_wr} started including conference
publications which contributed 7.5\% among 12 ranking factors in total~\cite{usnews_conf_2019}.

Regarding to the criteria of \textbf{O}peness and \textbf{T}ransparence, the
data used for assessing institutions should be publicly accessible at least for
verification from other neutral parties. Furthermore, the methodology should be
transparent such that an external user using open source data and published method
can reproduce the final result. Following the \acrshort{usn_wr} example above, \acrshort{usn_wr}
used bibliographic data provided by \gls{wos} which is a paid-access platform.
Hence, only fee-paying users can access the database. Moreover, the list of venues
is not public. It could be seen that is an example of closed source data and
questionable methodology.

In terms of the \textbf{O}bjectiveness of data, all quantitative indicators should be
meaningful metrics and have a logical foundation for comparisons. For instance,
publications, citations, honors, and funding are considered objective data~\cite{GOTO_ranking}.

At the time of writing, there are three GOTO rankings for the computer science field:
CSMetrics, CSIndexbr, and CSRankings. Without regard to departmental organization or
authors' job titles, CSMetrics is a quantitative metric-based
ranking focusing on the institution as a whole. CSIndexbr provides transparent and meaningful
statistics about the Brazillian scientific production in computer science field.
CSRankings is a metric-based ranking that is faculty-centric and based on
publications at selected top conferences. There is no a perfect ranking which
serves all purposes effectively; thus, those GOTO rankings are created to support
each other and solve different types of issues. Among those three GOTO rankings,
CSRankings is the one which I focus on in this thesis.

\section{CSRankings}
% Difference of CSRankings from two remaning GOTO rankings

In terms of measuring the quality of academic papers, citation-based metrics are
considered suitable tools provided that they are accurate and used with care and
competence~\cite{citation_based_agree}. The logical basis is that papers with a high
number of citations have a greater influence and consequently higher value~\cite{citation_based_question}.
However, this principle is found unreliable and prone to control. A study shows that
32\% of the group of highly cited articles on clinical trials produced results which were
later contradicted~\cite{contradicted_highly_cited_papers}. Additionally, citation data
is not freely available, and studies showed that bibliometric indicators offered by citation measuring
systems, e.g., Google Scholar, may be easily manipulated~\cite{google_scholar_trick,
most_cited_author}.
On top of this, there is a phenomenon known as ``citation cartels'': small groups of
researchers band together to cite each other's papers, misleading the citation system
into assessing that their publications are highly influential~\cite{citation_cartels}.
This phenomenon could affect the scientific community by rewarding
quantity over quality~\cite{citation_cartels}.

Avoiding the mentioned disadvantages, CSRankings
ranks higher education institutions by their presence at top esteemed conferences. This approach is
considered not only incentive-aligned but also difficult to game, encouraging
faculties to publish high-quality articles at top venues~\cite{csranking_faq}.
Furthermore, as a GOTO ranking, CSRankings is entirely metric-based and transparent;
therefore, all source code and data are published so that anyone can freely access through
this link: \url{https://github.com/emeryberger/csrakings}. \autoref{fig:csrankings_frontpage}
and \autoref{fig:csrankings_faculty} show the user interface of CSRankings.

\begin{figure}
    \centering
    \includegraphics[height=\textheight,width=\textwidth,keepaspectratio]{
        figures/CSRankings_frontpage.png}
    \caption{Institution ranking within all areas in the world.}\label{fig:csrankings_frontpage}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[height=\textheight,width=\textwidth,keepaspectratio]{
        figures/CSRankings_show_people.png}
    \caption{Institution ranking within all areas in the world --- showing adjusted
    count and areas of each author.}\label{fig:csrankings_faculty}
\end{figure}

\subsection{Methodology}
%
CSRankings ranks institutions based on two indexes: \emph{`adjusted counts'} and \emph{`average counts'}.
From the viewpoint of faculty, the \emph{`adjusted counts'} is computed as the sum of \emph{`adjusted credit'}
which is divided uniformly across all co-authors in each publication. Hence, simply adding authors to an article
cannot increase rankings. In more detail, once an article is published at a selected-top-tier
conference, a faculty member obtains \(\frac{1}{N}\) credit for that paper, where \(N\) is
the number of authors regardless of their presence in the database of CSRankings. Thus,
the achieved credit is stable. If all authors of the published paper are in the database,
the maximum point of the paper is recorded as 1 credit. However, researchers pointed
out that the technique of adjusting count can lead to a situation discouraging
undergraduate students involved in research works. Furthermore, this approach could create
a disincentive against collaborating across institutions~\cite{divide_credit}. Many
contributors proposed an approach that only divides credits over authors who are in the
database. According to~\cite{csranking_faq}, however, such approach could have several
disadvantages:

\begin{itemize}
    \item It would be challenging to manually calculate the authorships.
    \item The credits of authors would be unstable, changing over time. When someone is
    removed from the database, the credit of other authors would increase.
    \item It would be an encouragement for senior researchers to collaborate with junior students
    who are not offered tenure.
    \item It would encourage to collaborate with industrial researchers, who are not presented
    in the database, instead of academic people, since authors do not have to share their
    credits.
    \item It would create a disincentive for senior faculty to see their junior co-authors
    get faculty appointments as they have to share credits when their co-authors present
    in the database.
\end{itemize}

From the viewpoint of institution, \emph{`average counts'} is an output of the
geometric mean of \emph{`adjusted counts'} per area:

\begin{align*}
    averageCount = \sqrt[N]{\prod_{i=1}^{N}(adjustedCounts_{i} + 1)},
\end{align*}

where \(N\) is an amount of areas selected. By using geometric mean, the only correct method
of averaging normalized results, the publication rates and sizes of areas are normalized~\cite{geometric_mean}.
Moreover, only articles at least 6-pages long are taken into account. The rationale
behind this constraint is stated by the maintainer of CSRankings, Emery Berger:
``Conference proceedings often include things like keynotes, posters, and demos, which are
all shorter than normal papers. These are generally captured by this page limit.''~\cite{constraint_6_pages}.

Since only papers accepted at top conferences are recorded in the CSRankings system,
the policy of selecting those prestigious conferences is unavoidably controversial.
The list of selected conferences in each area is constituted based on the results of surveys
recording assessments of faculty across a variety of universities. Each participant of
the survey is requested to grade pre-selected conferences on the scale of 1 to 5, \emph{Strongly
disagree to Strongly agree} respectively (see \autoref{fig:sec_survey}). There is a range
of opposite viewpoints around this venue-selecting approach. In the scope of this thesis,
I do not conclude which method is better. As CSRankings is an open source project, everyone is encouraged
to contribute their viewpoints. For instance, through an issue reporting system, researchers requested
inserting a top-tier venue~\cite{add_NDSS},
or suggested removing a conference reconsidering its top-tier status~\cite{remove_INFOCOM}.

\begin{figure}
    \centering
    \includegraphics[height=\textheight,width=\textwidth,keepaspectratio]{
                    figures/conference_survey.png}
    \caption{The survey result indicating top tier venue in Security field.}\label{fig:sec_survey}
\end{figure}

\section{dblp}
%
dblp, a web server providing bibliographic metadata
and linking to electronic versions of computer science articles, is a bibliometric
data provider for CSRankings. The dataset of dblp is freely accessible and
high-quality; thus, it has become a widely-used tool for measuring the academic
performance of researchers or institutions~\cite{dblp_vs_citeSeer}.
Initially, it was a digital library operated by
the \emph{database systems and logic programming (dblp)} research group at University
of Trier from 1993 to the end of 2010~\cite{dblp_acronym}. From 2010 to 2018, dblp was
a joint service of the University of Trier and Schloss Dagstuhl --- Leibniz Center for
Informatics~\cite{dblp_institution_behind}. Since November 2018, Schloss Dagstuhl has
been the main operator collaborating with the University of Trier. In addition to the two
aforementioned contributors, dblp has received grants by several donors:
Deutsche Forschungsgemeinschaft (German Research Foundation), Leibniz Gemeinschaft
(Leibniz Association), Klaus Tschira Stiftung gGmbH (Klaus Tschira Foundation),
Allen Institute for Artificial Intelligence (AI2), Microsoft Research, and the Very
Large Data Base Endowment~\cite{dblp_institution_behind}.
At some times, the name ``Digital bibliography \& Library Project'' was seen as a
backronym of dblp, but this name is not in use now~\cite{dblp_acronym}. The correct name
of dblp is ``dblp'', or ``dblp computer science bibliography''~\cite{dblp_acronym}.

With respect to the completeness of dblp, there are at least two scientific studies regarding
it~\cite{dblp_vs_citeSeer,dblp_coverage}. However, their analyses were conducted more than
10 years ago, not reflecting the current degree of coverage of dblp database.
A study shows that approximately 24\% of all publications in the computer
science field are indexed by dblp~\cite{dblp_vs_citeSeer}. In terms of sub-field coverage,
dblp was supposed to store publications only from the area
of database systems and logic programming at the beginning~\cite{dblp_acronym}. However,
it has extended continuously and included all areas of computer science~\cite{dblp_acronym}.
This scope extension started after the year 2000 with the support of large number of common
authors~\cite{dblp_coverage}.
As publications at conferences have a main impact on bibliometric studies in the computer
science field, the coverage of conference is a vital factor of any bibliographic
data services focusing on the computer science field~\cite{dblp_brazil_coverage}. In case
of dblp, the conference coverage of all sub-fields was around 65\% in average at the
end of 2005~\cite{dblp_brazil_coverage}. However, dblp is still
incomplete, and true comprehensiveness can never be reached~\cite{dblp_faq_completeness}.

At the time of writing, dblp contains more than 6,500,000 bibliographic entries.
From 2019 to 2022, dblp inserted 500,000 new entries per year in average
(see \autoref{fig:new_record_stat}). Anyone can freely access to the raw dataset of dblp via
\url{https://dblp.org/xml/dblp.xml.gz}.

\begin{figure}
    \centering
    \includegraphics[height=\textheight,width=\textwidth,keepaspectratio]
    {figures/dblp_new_records_per_year.png}
    \caption{New dblp records per year~\cite{dblp_new_records_stat}.}\label{fig:new_record_stat}
\end{figure}

\subsection{Structure of records}

Being started as a simple server for testing web technology, dblp was a minimal
collection of \gls{tocs} of significant publications from the fields of
database systems and logic programming~\cite{dblp_evolution}. These \gls{tocs} were inserted
and formatted in
static HTML, with a few introduction pages manually added~\cite{dblp_evolution}. The system
design of dblp was lean, emphasizing the simplicity and manageability. It was operated with
a minimal amount of custom scripts and without the support of a database management system.

Since publication data was stored as a collection of HTML-based \gls{tocs}, the data structure
was homogenous~\cite{dblp_evolution}. Each person in the dblp database has their own author
page. Author pages listing all academic papers (co)authored % chktex 36
by a person were generated in two steps. In the first stage, the HTML parser based on the
discontinued xmosaic browser was compiled with a shell script, in which identifying
information was hard-coded. The output of the compilation was a large line oriented file called ``TOC\_OUT''.
In the next stage, the program ``mkauthors'' read ``TOC\_OUT'' into a main memory data
structure and produced HTML files of author pages, an index of all author pages, and
the ``AUTHORS'' file storing all author names~\cite{dblp_evolution}. According to~\cite{dblp_lessons},
the typical HTML page looked like this:

\begin{minted}{xml}
    <h2>Keynote Addresses</h2>
    <ul>
    <li><cite key="conf/vldb/Jhingran06" style=ee>
    <li><cite key="conf/vldb/Sikka06" style=ee>
    </ul>
    <h2>Ten-Year Best Paper Award Talk Session</h2>
    <ul>
    <li><cite key="conf/vldb/HalevyRO06" style=ee>
    </ul>
    <h2>Research Sessions</h2>
    <h3>Continuous Query Processing</h3>
    <ul>
    <li><cite key="conf/vldb/LiCTACH06" style=ee>
    <footer>
\end{minted}

Due to the growth of the amount of bibliographic information, HTML-based tables of
contents became an unreliable design. In order to manage the increasing amount of
citation linking, annotated bibliographies, etc., publications assigned with unique
IDs should be stored in the more bibliographic records~\cite{dblp_evolution}. The
appearance of the new \gls{xml} format at that time, in 1994, was the solution for the
need of changing the structure of files storing bibliographic records. As a result,
the dblp \gls{xml} record was able to easily adapt to the new \gls{xml} framework
with only minor syntactic modifications. A dblp \gls{xml} record looked like this:


\begin{minted}{xml}
    <article key="journals/sigmobile/AthalyeBKMZ20" mdate="2020-11-04">
    <author pid="203/8790">Anish Athalye</author>
    <author pid="14/8279">Adam Belay</author>
    <author pid="k/MFransKaashoek">M. Frans Kaashoek</author>
    <author pid="82/11191">Robert Tappan Morris</author>
    <author pid="99/5780">Nickolai Zeldovich</author>
    <title>Notary: A Device for Secure Transaction Approval.</title>
    <pages>34-38</pages>
    <year>2020</year>
    <volume>24</volume>
    <journal>GetMobile Mob. Comput. Commun.</journal>
    <number>2</number>
    <ee>https://doi.org/10.1145/3427384.3427395</ee>
    <url>
    db/journals/sigmobile/sigmobile24.html#AthalyeBKMZ20
    </url>
    </article>
\end{minted}

In addition to a publication record as shown above, a person record, storing supplemental
information of author, is also generated as the following format:

\begin{minted}{xml}
    <person key="homepages/99/5780" mdate="2016-11-11">
    <author pid="99/5780">Nickolai Zeldovich</author>
    <note type="affiliation">
    Massachusetts Institute of Technology, Cambridge, USA
    </note>
    <url>
    https://scholar.google.com/citations?user=DJ6--hMAAAAJ
    </url>
    </person>
\end{minted}

\subsection{Author name ambiguity}
%
The author name ambiguity is a common and unavoidable problem in digital
libraries~\cite{population_model_dblp_case_study,dblp_accuracy_name_ambiguation,
collaboration_CS}. Names may not be adequate to differentiate one person from another
in many circumstances due to two problems: homonym and synonym~\cite{bibliometric_name_ambiguity}.

Homonyms are a collection of words that have similar spelling and pronunciation but
diverse meanings~\cite{homonym}. In the context of dblp, when an author's name is
queried, it is not guaranteed that only one matching result is returned. It is
common in dblp that two or more authors share the same name. For example, Thomas Olsson,
a common Swedish name, matches two different person records in dblp.\ In order to
make homonyms distinguishable, a string of four digits is appended to the names:

\begin{minted}{xml}
    <author>Thomas Olsson 0001</author>
    <author>Thomas Olsson 0002</author>
\end{minted}

Another issue while dealing with personal names is the synonym problem, different
words have similar or identical meanings. In case of dblp, an author can have
many different names. The variant of name is classified by the permutations of
name parts \textbf{(Li Chen \(\sim~\) Chen Li)}, the diacritical mark
\textbf{(Ren\'{e} \(\sim~\) Rene)}, or the expanded initial \textbf{(M. Ley \(\sim~\) Michael Ley)}
~\cite{dblp_lessons}. To represent multiple alias names, the name variants are inserted in
addition to the primary name, the first <author> element, in the person record:

\begin{minted}{xml}
    <person key="homepages/d/MargaretHDunham" mdate="2020-07-09">
    <author pid="d/MargaretHDunham">Margaret H. Dunham</author>
    <author pid="d/MargaretHDunham">Margaret H. Eich</author>
    <note type="affiliation">Southern Methodist University, Dallas,
    Texas, USA</note>
    <url>http://engr.smu.edu/~mhd/</url>
    <url>https://dl.acm.org/profile/81409595451</url>
    </person>
\end{minted}

To identify the name issues described above, dblp uses simple heuristics on the collaboration
indexing~\cite{dblp_lessons, collaboration_CS,population_model_dblp_case_study}.
dblp colors the author and co-authors with the same color. If there is no direct collaboration
between two authors, or indirect collaboration through a common co-author, they
are assigned different colors. If the co-author list is monochrome,
the main name entry represents a single author~\cite{dblp_lessons,population_model_dblp_case_study}.
Otherwise, if a name entry includes multiple authors with the same color, it could be a candidate
for homonym, and further investigation or splitting may be necessary. The requests for splitting
are typically initiated by authors who notice their academic papers mixed with publications of
other authors. Besides, in some cases, maintainers of dblp can also identify the need to split
a name entry. However, homonyms still remain undetected in dblp database.
To address issues with synonyms, dblp uses a software to compare the names of two authors in many
variants. If two authors have never collaborated directly but have common publications,
their names are marked as synonyms~\cite{collaboration_CS}.

The author name ambiguity may not be a significant problem for users who query
authors' name for occasional purposes. However, it is a crucial issue for those who
use bibliographic information platforms for accumulating academic knowledge to conduct
a research and acquiring practical insights to make decisions about recruiting or
funding~\cite{do_metric_matters,performance_based_funding}.

Despite the fact that many ambiguous names are still not properly distinguished, dblp
shows good performance in disambiguating author names~\cite{dblp_accuracy_name_ambiguation}.

\subsection{Person IDs}
%
So far, dblp has been widely used as a source of reference by conference servers, preprint
servers, publishers, and universities~\cite{dblp_lessons}. In order to be an optimal data
provider for these clients, the \acrshort{url}s of
person records, which cannot contain variable elements such as person name, should be stable.
Hence, creating a unique ID for each person in dblp database is necessary. \gls{pid}
without the ``homepages/'' prefix, also an ID of each person record, is used to map
to an author page via the following \acrshort{url}:\

\begin{verbatim}
    https://dblp.org/pid/<PID>.xml
\end{verbatim}

where \emph{<PID>} is the person record ID of the target author.

\section{Version control with Git}
%
CSRankings is an open source project which is hosted on GitHub; therefore, anyone
can contribute to it through the pull-request protocol. In this section, essential terms
and definitions related to version control models will be introduced. Firstly,
Version Control is a system that logs the changes of files over time so that these files
can be reverted to a specific state later. In addition to the changes
over time, a \gls{vcs} also records who modified the file and when, who
introduced the issue and when, and more~\cite{git_book}.

According to a survey of 820 developers~\cite{dvcs}, in 2014, 65\% utilized \gls{dvcs} and 35\%
used \gls{cvcs}. In \acrshort{cvcs}, there is a single server that hosts all
versioned files within a project, and users work on files from that central
codebase. There are some products implementing that client-server approach such as CVS,
Subversion, and Perforce. On the other hand, \acrshort{dvcs} is a peer-to-peer
approach in which each user stores the full history of a project on their
local devices. According to~\cite{dvcs}, this distributed system offers many
advantages over \acrshort{cvcs}:

\begin{itemize}
    \item Developers can work independently on their local copies of repositories,
    allowing them to work offline while maintaining the full project history.
    \item Developers can effortlessly create and merge branches at a low cost.
    \item Developers can commit only the lines of a file that have changed, as
    opposed to having to commit the entire file as in \acrshort{cvcs}.
\end{itemize}

There are many widely-used \acrshort{dvcs}s such as Git, Darcs, Mercurial, and Bazaar.
In this thesis, I chose Git to focus on, as component files of CSRankings are versioned
with Git.

\subsection{Git}
%
According to~\cite{git_stat}, the usage of Git was reported by 93.87\% of the
participants surveyed in 2022. This indicates beyond doubt that
Git is the most widely used \gls{dvcs}. By using Git, contributors
fully mirror the shared repository rather than capturing only the latest changes
of files~\cite{git_article,git_book}. It does not keep track of version
controlled files as a series of changes, but instead as a sequence of snapshots. This means
that Git takes snapshots of all files under a version controlled project once a user
saves the state of the project.
\autoref{fig:snapshots_git} visualizes the way Git manages versions of files.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]
    {figures/Git_Snapshot.png}
    \caption{Git stores a series of snapshots.}\label{fig:snapshots_git}
\end{figure}

Git has a significant amount of commands which can be studied further through its
\href{https://git-scm.com/docs/git}{document}. In the scope of this thesis, the
following commands were mainly used:

\begin{itemize}
    \item \textbf{git fetch}: download the latest version of a remote repository
    for examining without applying the latest changes to the local repository. A user
    can fetch from any configured, or even non-configured, remote. Explicitly pointing to
    which remote to be fetched is optional as Git uses some heuristics to figure out what
    is the default remote from the context in which \textbf{`git fetch'} was called.
    \item \textbf{git add}: begin tracking the latest local changes (newly added files,
    last modified files) which are not currently captured by Git.
    \item \textbf{git commit}: with this command, a commit is created, recording a snapshot
    including all changes tracked via \textbf{`git add'}. A commit records a snapshot of a
    version controlled project which can be
    reverted to a previous stage or compared to later. The snapshot is still on the local
    machine that creates the commit. To sync the local snapshot to a remote repository,
    \textbf{`git push'} needs to be executed.
    The commit is labeled by its \gls{sha1} checksum which is calculated from the state of a
    repository, including the hash of all files in the repository, the hash of the previous commit,
    date and time, etc.
    \item \textbf{git pull}: copy remote changes to the local repository, detecting that
    there are no conflicting modifications after local changes are applied.
    \item \textbf{git push}: upload local commits to a remote repository, from which they can
    further be distributed to the local repositories of other developers.
\end{itemize}

% Git branching
With regard to the stability of a project, branching is utilized to avoid accidentally
raising errors in the working version of the project. The idea of branching is to separate the main
line of development from what changes are going to be generated. When \textbf{`git commit'} is executed,
Git saves a commit object containing the reference to previous commits (parent commits)~\cite{git_book}. Thus,
a branch is simply a portable reference to one of existing commits~\cite{git_book}. In terms of
tracking which local branch a contributor is working on, Git maintains a \emph{HEAD} pointer.
In addition, ``main'', or ``master'', is the default branch initialized when
a new repository is created. These default branches are functionally equivalent to other branches.
In other words, all branches are technically identical within a project.
For illustrative purposes, commits are labeled as \(C_i\), where \emph{i} is \(1,\ldots ,4\),
in \autoref{fig:div_branch}.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]
    {figures/Branching.png}
    \caption{Two branches pointing to different commits.}\label{fig:div_branch}
\end{figure}

% topic branch
There are several branching workflows which are widely used in practice. However,
topic branch is the reliable and effective workflow used in this work. A
topic branch is a short-lived branch that is created and used for a single particular
function or related features~\cite{git_book}. For example, a new log-in function is
developed in a web application. A topic branch called ``log-in\_function'' is created
and diverges from the ``main'' branch, the official working version of the application
(see \autoref{fig:topic_branch}).
On the new branch, developers can make changes without damaging the ``main'' line.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]
    {figures/topic_branch.png}
    \caption{A topic branch besides the main (default) branch.}\label{fig:topic_branch}
\end{figure}

Once the newly developed feature, log-in function in this example, is ready to be
integrated into the web application, a merge command is executed. As a result,
a merge commit, which has at least two predecessors, is created on top of the stack
of commits (see \autoref{fig:merge_topic_branch}).

\begin{figure}
    \centering
    \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]
    {figures/merge_branch.png}
    \caption{History after merging ``log-in\_function''.}\label{fig:merge_topic_branch}
\end{figure}

\subsection{GitHub}\label{subsec:github}
%
GitHub, a web-based hosting service for version control using Git, is utilized by many
open source projects for Git hosting, access control, bug tracking, and collaborative coding~\cite{git_book}.
Beside hosting Git repositories, GitHub provides additional features: collaborative
coding, automation and \gls{ci}/\gls{cd}, security scanning, project management, and
team administration. Founded in 2008, GitHub has been a subsidiary of Microsoft since 2018.

Git and GitHub are sometimes interchangeably used, but they are different in a
number of aspects. Git is a \gls{dvcs} which is installed on local devices. Developers
use Git to keep history of changes on their local storage. In contrast, GitHub is
a cloud-based service in which developers can share their code to others, and other
developers can contribute to others' code as well. GitHub can be seen as a centralized
location for hosting copies of local Git repositories. In terms of user command, Git
focuses on command-line tools, including \textbf{git add}, \textbf{git commit}, \textbf{git push},
etc. On the other hand, GitHub serves graphical interfaces where tasks are performed.
In addition to functions of version control, GitHub offers various administration tools,
collaboration features, integration tools, and a wide range of external plugins.
GitHub is designed to work with Git, meaning that GitHub cannot operate properly
without Git.

In this work, since the main task is to contribute to the CSRankings GitHub
repository, I will discuss the \emph{collaborative coding} feature in more detail.
A \gls{pr} is the mechanism through which contributors can submit changes for inclusion
in a repository owned by another user or organization~\cite{github_glossary}. Each \acrshort{pr} has
its own discussion forum where contributors and owner can comment on it, illustrated
in \autoref{fig:pull_request}. As a result, the repository owner can accept or reject
a \acrshort{pr}.

According to~\cite{git_book}, there is a collaboration workflow which GitHub users follow
while contributing to an open source project:

\begin{enumerate}
    \item Fork the project. The term ``fork'' indicates the act of copying a project, which a contributor do not
    have push access, to their namespace for later contributing~\cite{git_book}. After
    pushing changes to the personal fork of the project, the contributor can open a
    \acrshort{pr} to inform others about the changes. Once it is opened, the contributor
    can discuss and review the potential changes with collaborators and add supplemental
    commits if necessary before these changes are merged into the original repository~\cite{github_PR_doc}.
    \item Create a topic/function branch from the \textbf{``main''} branch.
    \item Edit files and create commits.
    \item Push this branch to the personal GitHub repository of the contributor.
    \item Open a \acrshort{pr} on GitHub against the original `upstream' project.
    \item Discuss, and continue committing if it is necessary.
    \item The owner of the original `upstream' project merges or closes the \acrshort{pr}.
    \item Sync the updated \textbf{``main''} branch back to the fork of the contributor.
\end{enumerate}

The next subsection covers other \acrshort{dvcs} hosting services which are similar to GitHub.

\subsection{GitHub and its alternatives}
%
Besides GitHub, there are many other Git hosting services, including, for example, GitLab, Bitbucket,
and SourceForge. Among them, GitLab has gained significant attention. GitLab, an
open source web-based service for Git repositories, provides
collaborative and end-to-end software development platform with built-in
version control. Developed by GitLab Inc, GitLab was launched in 2011. In addition
to source code management, GitLab offers other key features: team planning, continuous
integration, package registry, code review workflow, fuzz testing, continuous delivery,
error tracking, Kubernetes management, vulnerability management, etc.

Both GitHub and GitLab are based on Git, making it simple for developers
to migrate their code seamlessly across the two platforms.
In addition, they share many common features. GitHub and GitLab
both provide issue tracking tools that allow for status modifications and
the assignment of owners to each issue. On both platforms, developers
can add a description or comment to issues or Merge/Pull Requests. Both
of them maintain a separate system for documentation known as Wiki and is
built into each project as a separate Git repository. On the other hand,
both platforms offer similar functions but with different terminologies
(see \autoref{tab:dif_term_same_func}). There are still
many similarities of both services from different aspects, but they are
beyond the scope of this thesis.

GitLab and GitHub have many key differences which fulfill requirements of
different projects. Hence, developers
need to consider the scope, structure, and resource of their products before
selecting the appropriate platform.

\begin{table}
    \centering
    \begin{tabularx}{\linewidth}{|X|X|X|}
        \hline
        GitHub & GitLab & Meaning\\
        \hline
        Pull Request & Merge Request & Request to integrate a branch to another branch.\\
        \hline
        Gist & Snippet & Instantly shared code.\\
        \hline
        Repository & Project & Container storing all project's files and each file's
        revision history, and project-specific settings.\\
        \hline
        Organization & Group & Shared accounts to manage many related-projects at once.\\
        \hline
    \end{tabularx}
    \caption{Different terminologies have similar functions in GitHub and GitLab.}
    \label{tab:dif_term_same_func} % chktex 24
\end{table}