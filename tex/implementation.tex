In order to integrate the CS Unit of \acrshort{tau} into the CSrankings system, many requirements
must be followed. All detailed instructions are publicly accessible through the
\href{https://github.com/emeryberger/CSrankings}{Git repository of CSRankings}.
According to~\cite{contributing_rule}, there are four important guidelines that
should be noticed:

% \thispagestyle{empty}
% { % create a group so the change is local
% \renewcommand{\theenumi}{P\arabic{enumi}}
\begin{enumerate}[label*={\textbf{GL\arabic*}}, ref={GL}\arabic*]
    \item Do not modify any files except \emph{csrankings-[a-z].csv} or (if needed)
    \emph{country-info.csv}.\label{rule:add_country}
    \item Make sure the Google Scholar IDs are just the alphanumeric identifier (not
    a URL or with \&hl=en).\label{rule:google_id}
    \item Check to make sure the home page is correct.\label{rule:homepage}
    \item Check to make sure the name corresponds to the dblp entry (look it up at
    \url{http://dblp.org}).\label{rule:dblp_name}
    \item Insert new faculty in alphabetical order (not at the end) in the appropriate
    \emph{csrankings-[a-z].csv} files. Do not modify \emph{csrankings.csv}, which is
    auto-generated.\label{rule:add_alphabet}
\end{enumerate}

In this work, I mainly analyzed these \gls{csv} files: \emph{csrankings.csv}, \emph{master.csv},
\emph{csrankings-[a-z].csv}, and \emph{country-info.csv}. Among these files,
\emph{csra-\\nkings-[a-z].csv} is a set of alphabetical-sorted files starting from
\emph{csrankings-a.csv} to \emph{csrankings-z.csv}. In case of \emph{master.csv},
we created it to keep track of the information of personnel in the CS Unit within the Faculty of Information
Technology and Communication Science at \gls{tau}. Moreover, \emph{master.csv} was created in a
separate repository meant to host the work related to this project, not being part of CSRankings repository.
On the other hand, other \acrshort{csv}
files are managed by the maintainers of CSRankings. However, everyone can contribute
to them through a \acrshort{pr} which is also the final step in the integration phase.
The links of aforementioned files are listed in \autoref{tab:csv_file_links}.

\begin{table}
    \begin{tabularx}{\linewidth}{|X|X|}
        \hline
        File & Link\\
        \hline
        master.csv & \url{https://gitlab.com/nisec/bibliometrics/
        -/blob/main/master.csv}\\
        \hline
        csrankings.csv & \url{https://github.com/emeryberger/CS
        rankings/blob/gh-pages/csrankings.csv}\\
        \hline
        country-info.csv & \url{https://github.com/emeryberger/CS
        rankings/blob/gh-pages/country-info.csv}\\
        \hline
        csrankings[a-z].csv & \url{https://github.com/emeryberger/CSrankings}\\
        \hline
    \end{tabularx}
    \caption{Links of aforementioned CSV files.}\label{tab:csv_file_links}
\end{table}

In this chapter, I will discuss the contributing guidelines, integration steps, and
their motivation. Each integration step that is introduced below addresses the
corresponding guideline.

\begin{filecontents*}[overwrite]{country.csv}
institution,region,countryabbrv
AUEB,europe,gr
Aalborg University,europe,dk
Aalto University,europe,fi
Aarhus University,europe,dk
Aberystwyth University,europe,uk
\end{filecontents*}

% starting the section
\section{Data collecting and processing}

According to GL1, a new entry needs to be inserted into \emph{country-info.csv} when the
home institution
is not in the USA\@. Since \acrshort{tau} locates in Finland, I needed to add its entry to
\emph{country-info.csv}. \autoref{tab:country-info} shows the
format of entries in \emph{country-info.csv}.

\begin{table*}[!ht]
 \csvreader[tabular =|p{0.5\linewidth}|p{0.2\linewidth}|p{0.2\linewidth}|,
   table head=\hline institution & region & countryabbrv\\\hline,
   late after line=\\\hline]%
  {country.csv}{institution=\insti, region=\reg, countryabbrv=\abbr}%
  {\insti\ & \reg\ & \abbr}%
 \caption{The first 5 records of the country-info.csv.}\label{tab:country-info}
\end{table*}

Following the above format, I inserted the \acrshort{tau} entry as:
\begin{center}
 \begin{BVerbatim}
  Tampere University,europe,fi
 \end{BVerbatim}
\end{center}

With regard to faculty inclusion, each entry included in the CSRankings system must contain
required fields: \emph{name (dblp name)}, \emph{affiliation}, \emph{homepage}, and
\emph{scholarid}.
In this work, \emph{affiliation} of all faculty entries are ``Tampere University''.
In addition, each eligible faculty must be a full-time, tenure-track member
who can solely supervise PhD students in the computer science field~\cite{contributing_rule}.
To help clarifying this requirement, I added the \emph{job title} field beside required fields
in our internal \emph{master.csv} file.

% Billy provided a list of faculty in \gls{cs} Unit from which I need to acquire
% additional information to meet the requirement of CSRankings.

%
Following GL2 and GL3, I manually searched Google Scholar IDs and homepages of faculty
through the Google search engine. By scanning the homepage, I can collect the
\emph{job title} of a particular researcher as well. Most researchers at \acrshort{tau}
have their homepages in the form as:

\begin{center}
    \begin{BVerbatim}
        https://www.tuni.fi/en/<full-name>
    \end{BVerbatim}
\end{center}

e.g. \url{https://www.tuni.fi/en/karen-eguiazarian}. However, some people
have their homepages in the alternative form as:

\begin{center}
    \begin{BVerbatim}
        https://researchportal.tuni.fi/en/persons/<full-name>
    \end{BVerbatim}
\end{center}

e.g. \url{https://researchportal.tuni.fi/en/persons/frank-emmert-streib}.
In order to ensure that these two \gls{url} patterns are not mistakenly used,
I built a script that checks the status code of the \gls{http} request. If
the returned status code is 200, the recorded homepage is accessible.

In order to double-check Google Scholar IDs, I utilized the Python module
known as \emph{scholarly}\footnote{\url{https://github.com/scholarly-python
-package/scholarly}}. This open source module allows users to retrieve
author and publication information from Google Scholar. In this work,
\emph{scholarly} helped me
return a corresponding author name with a given Google Scholar ID\@. By comparing
the returned author name from \emph{scholarly} and the full name, I can
detect whether or not the collected Google Scholar ID refers to the intended
author.

According to GL4, the \emph{name} field of faculty is the name corresponding
to a dblp entry. In some cases, the name of a faculty member is not consistent with their name in
the dblp database. For example, Prof.\ \textbf{Davide Taibi} has his corresponding dblp
name as \textbf{Davide Taibi 0001}.\ dblp appends numbers to a person name to solve
the homonym problem in which many people have the same full name.
On the other hand, a person can have more than one corresponding full names, known as
the synonym problem. For example, the homepage displays \textbf{Karen Eguiazarian} as his full name,
but his name in dblp is recorded as \textbf{Karen O. Egiazarian}. To address these issues,
I maintained two separate fields of name, one for the ``normal'' name and another for the
name in dblp system, to track the consistence between them.

dblp provides an efficient searching function so that I can easily find a
dblp author entry by inputting the full name of a particular researcher. In most cases,
there is only one matching result returned. On the other hand, in some cases,
dblp returns more than one entry. In these cases, I selected the entry
corresponding to the Tampere University \emph{affiliation}. If the field of \emph{affiliation}
was undefined, I scanned the list of published articles of each returned entry and compared
to the list on author's Google Scholar page. I resolved conflicts by considering
the author entry that had more articles matching those on the author's Google Scholar
page to be correct.

Once a dblp author record was defined, I effortlessly collected the \acrshort{pid} of
an author through the corresponding \gls{url} of the dblp homepage:

% In terms of the dblp-related information,
% dblp name and Person ID (\gls{pid}),
% a given ``normal'' name is inserted in the search bar of dblp website, usually only one
% match record is returned. In the complicated case, various results are returned, I compare
% publications of each ``Person Record'' returned to the list of articles published on his/her
% Google Scholar page. The rule here is that to choose the record that has the list of
% publications overlapping mostly articles on the corresponding Google Scholar page. Once the
% dblp record is defined, the \gls{pid} of the researcher is obtained effortlessly through the
% \gls{url} of the dblp homepage corresponding to the returned ``dblp author page'':

\begin{center}
    \begin{BVerbatim}
        https://dblp.org/pid/e/KOEgiazarian.html
    \end{BVerbatim}
\end{center}

where the \acrshort{pid} is ``e/KOEgiazarian''. To avoid
capturing the wrong \gls{pid} by mistake, I built a script that validated each \acrshort{pid}
and its \emph{dblp name}. Firstly, it requested an \gls{xml} version of a \emph{Person Record}
through the following pattern:

\begin{center}
    \begin{BVerbatim}
        https://dblp.org/pid/e/KOEgiazarian.xml
    \end{BVerbatim}
\end{center}

where the dblp server stored:
\begin{minted}{xml}
    <dblpperson name="Karen O. Egiazarian" pid="e/KOEgiazarian" n="360">
        <person key="homepages/e/KOEgiazarian" mdate="2022-02-15">
            <author pid="e/KOEgiazarian">Karen O. Egiazarian</author>
        <! -- more irrelevant tags are skipped here -->
    </dblpperson>
\end{minted}

Then by comparing the value of the attribute \emph{name} in the \emph{dblpperson}
element to the \emph{dblp name} returned from the search function,
I can know whether or not the collected \emph{dblp name} and \acrshort{pid}
point to the same dblp entry.

\section{Preparing a Pull Request}\label{sec:pull_request}
%
After all required fields were collected and checked, I inserted faculty
entries following the GL5.

Since a certain amount of dblp person names
contains non-unicode characters, records cannot be simply sorted by the built-in Python
sorting method.
Fortunately, CSrankings is an open source project; thus, its sorting method is publicly accessible. By
utilizing the script \emph{split-csrankings.py}\footnote{split-csrankings.py --- \url{https://github.
com/emeryberger/CSrankings/blob/gh-pages/util/split-csrankings.py}}, the sorting step becomes
unchallenging. The job of \emph{split-csrankings.py} is to apply the \emph{strip\_accents} function
to each record in \emph{csrankings.csv} and then sort them in alphabetical order. Therefore,
I only needed to append all eligible entries to the end of \emph{csrankings.csv} and execute
\emph{split-csrankings.py} script to generate a sorted range of \emph{csrankings-[a-z].csv}.
Following the GitHub workflow mentioned in \autoref{subsec:github}, I synced-up the local changes
to my remote fork of CSRankings repository. At this stage, a pull request is ready to be opened.

Receiving feedback from the maintainer of CSRankings, I created the follow-up commits
to address the following issues:

\begin{itemize}
    \item \href{https://github.com/ancuongnguyen07/CSrankings/commit/7cf758c94e966d740
    f24ca31b4a273e86ae13e97}{7cf758c9}: Update homepages which show both titles
    showing both titles: Academy Research Fellow and
    Associate Professor (tenure track).
    \item \href{https://github.com/ancuongnguyen07/CSrankings/commit/8acb67f0e83b8d6c0
    2a94eeda4c4cc8389578a94}{8acb67f0}: Replace dead homepage.
    \item \href{https://github.com/ancuongnguyen07/CSrankings/commit/136e8e1aa261dfb53
    dafc186aa7950514a2a0844}{136e8e1a}: Remove teaching faculty and visitors.
    \item \href{https://github.com/ancuongnguyen07/CSrankings/commit/7e449d5ddb8c1c712
    4d8711b7e36605e9d333d62}{7e449d5d}: Remove non-CS faculty.
\end{itemize}

As a result, the history of commits in my fork of CSRankings diverged, illustrated
in \autoref{fig:fork_branches}.

% focusing on only two branches ``gh-pages'' (the main branch of the CSRankings
% repository) and ``Tampere\_Uni\_CS'' (the branch which I made changes)

\begin{figure}
    \centering
    \includegraphics[width=\textwidth,height=\textheight,keepaspectratio]
    {figures/personal_fork_branch.png}
    \caption{History in my fork of the CSRankings repository.}\label{fig:fork_branches}
\end{figure}