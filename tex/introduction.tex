I joined the \emph{Bibliometrics} project through the \emph{Research Trainee} program in which
Bachelor students can freely choose an available project to obtain experience
in a research environment and earn credits. The first stage of the \emph{Bibliometrics}
project is to integrate personnel of the \gls{cs} Unit within the Faculty of Information
Technology and Communication Science at \gls{tau} into the CSRankings database.
I selected this project as it matched my academic background and
caught my interest as well. In later paragraphs, I will introduce the motivation
of this work and outline the structure of this thesis.

% Impact of Uni
For ages, universities have played an essential part in the evolution of civilizations
overseas through their teaching and research missions~\cite{review_of_uni_ranking}.
In addition to performing these duties, they also design development strategies and
contribute in increasing graduate employment,
raising the standard of education in society, expanding opportunities for individuals, and developing
knowledge and technology~\cite{review_of_uni_ranking}. Universities that are conceptualized
as knowledge-explorers, along with enterprises known as knowledge-exploiters, generate new
regional inventive capacities~\cite{uni_role}.

% The impact of uni ranking
As higher education institutions play a critical role in the social development,
it is necessary to have a robust method to measure their academic performance.
Hence, university rankings have been used since 1925, based on reputation of universities
within a nation~\cite{Shin2011_reputation_ranking}. In addition, the third era of globalization started
in 1989 and continues today, leading to the need of worldwide university rankings.
Hence, global university rankings first appeared in 2000s. For example, The Academic
Ranking of World Universities (ARWU) was first published in 2003 followed by THE (Times
Higher Education) World University Rankings in 2004. They have flourished in recent
years in spite of the criticism about their biased methodologies and objectives
~\cite{x_factor_ranking,global_ranking_reflection}. As a consequence of this
proliferation, global university rankings influence the means of people --- policy
makers, prospective students, investors, and university presidents --- evaluating
the quality of higher education institutions~\cite{ranking_battle,live_with_ranking,
review_of_uni_ranking}. In addition, university rankings have become an indicator
of economic competitiveness among countries since the more universities in a
particular nation or region that are rated among top 10, 50, or 100, the better the
country's or territory's economic repute and inventive potential~\cite{ranking_impact_eu}.
As a result, both developed and developing countries have invested a large portion
of their annual Gross Domestic Product (GDP) into higher education and research and
development (R\&D). For example, the European Union (EU) spent 328 billion euros on R\&D, 2.27\%
of GDP in 2021, while China and United States expended 2.4\% and 3.45\% respectively~\cite{rd_expense}.
It can be inferred that high-ranked universities in global rankings are likely to receive greater grants
or investments. Another impact of rankings on universities is that these benchmarks encourage the
internationalization, globalization, or multi-regional collaboration among educational institutions as many rankings
introduced the internationalization as a primary factor~\cite{global_ranking_internationalisation}.
In order to promote internationalization, several universities have started to use English as their
main instructional language~\cite{Shin2011_reputation_ranking}. In addition, universities
are encouraged to attract international academic staff and students to not only boost their
rankings but also increase their financial grants. With respect to prospective students,
university rankings are a vital factor they consider when they are choosing a university~\cite{choose_uni_ranking}.
Moreover, rankings are an effective tool for international students to select an university
to study abroad as it is challenging to visit an institution based overseas.
Given the above arguments, it is reasonable to state that global university rankings
have a significant impact on national systems, higher education institutions, and students.

However, major global university rankings --- Academic Rankings of World Universities (ARWU),
Times Higher Education (THE) World University Rankings, US News Best Global Universities
Rankings --- are reputation-based rankings whose methodology is subjective and unreliable.
Ranking indicators utilized by ARWU and THE rankings are most direct contributors to
reputational bias~\cite{inter_reputation_based_ranking}.
Hence, in this thesis, CSRankings, a metric-based ranking, is examined. CSRankings
evaluates academic performance of institutions in the computer science field based on their
contributions at top conferences. Furthermore, the process of merging personnel of the \acrshort{cs}
Unit of \acrshort{tau} into CSRankings is also presented in this thesis.
Once the \acrshort{cs} Unit of \acrshort{tau} is available on CSRankings, their academic
performance will attract more prospective students and researchers to join their
research community.

The remainder of the thesis is structured as follows. \autoref{ch:background} describes
the motivation, methodology, and technical background of CSRankings and its supporting
software. In \autoref{ch:related_work}, I will introduce \gls{gras} and emphasize the similarities
and differences between it and CSRankings. \autoref{ch:implementation} presents
in detail the process of integrating the \acrshort{cs} Unit of \acrshort{tau} into CSRankings.
\autoref{ch:result} provides the final result of the integration process and data analysis.
Finally, \autoref{ch:conclusion} summarizes the contents of mentioned chapters.