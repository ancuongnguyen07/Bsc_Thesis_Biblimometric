In addition to CSRankings, there are other university rankings that are based
solely on bibliometric data, including, most notably, \acrshort{ntu}
World University Ranking, \gls{urpa}, \acrshort{cwts} Leiden Ranking, and
\gls{gras}~\cite{Bibliometrics_ranking}. However, these
rankings are based on other indicators --- number of citations, h-index, etc. --- as
well, not only on the number of published articles.

\acrshort{ntu} World University Ranking assesses institutes based on three criteria,
including \emph{Research Productivity}, \emph{Research Impact}, and \emph{Research
Excellence}~\cite{NTURanking}. These criteria contribute different weights, ranging
from 10\% to 15\%, and each has a unique set of indicators~\cite{NTURanking}. A full
detail of its methodology can be found at \url{http://nturanking.csti.tw/methodoloyg
/indicators}.

\acrshort{urpa} ranks higher education institutions based on six indicators, including
\emph{Article}, \emph{Citation}, \emph{Total Document}, \emph{Article Impact Total},
\emph{Citation Impact Total}, and \emph{International Collaboration}~\cite{URAP}. These
indicators constitute a set of different weights, ranging from 10\% to 21\%,
on the overall ranking~\cite{URAP}. A full detail of its methodology can be found at
\url{https://urapcenter.org/Methodology}.

\acrshort{cwts} Leiden Ranking assesses universities based on four group of indicators
, including \emph{Scientific impact}, \emph{Collaboration}, \emph{Open access},
and \emph{Gender}~\cite{CWTS_Leiden}. Only core publications are counted while calculating
indicators~\cite{CWTS_Leiden}. A full detail of its methodology can be found at \url{https
://www.leidenranking.com/information/indicators}.

Among the above rankings, I selected \acrshort{gras} as the ranking which is studied
in this thesis. The reason behind this selection is that \acrshort{gras} is the
most influential university ranking which is non-reputation-based, and its methodology is
transparent and stable~\cite{analyze_shanghairanking,uncover_shanghairanking}.

\section{ShanghaiRanking's Global Ranking of Academic Subjects (GRAS)}

\acrshort{gras}, conducted by ShanghaiRanking Consultancy,
was published for the first time in 2009. In the first version, it ranked institutions
in 5 subjects, including Mathematics, Physics, Chemistry, Computer Science, and
Economics/Business. Over the years, the range of subjects has extended to 54 subjects in the latest
publication, \acrshort{gras} 2022. However, in order to build a correlation to CSRankings
which is a ranking focusing on computer science field, only the computer science subject is described
thoroughly in this thesis. Similar to CSRankings, \acrshort{gras} utilizes third-party data
providers, \gls{wos} and InCites, as a bibliometric database. With regard to the computer science
area, each database has its unique advantages. For example, dblp indexes the significant
number of unique publications, while \acrshort{wos} provides high-quality indexing and
bibliographic records in terms of accuracy, consistency, and relevance~\cite{best_bibli_database}.

\subsection{Methodology}
%
\acrshort{gras} 2022 only examines universities that have a certain number of publications
during the period of 2016--2020. The publication threshold varies in subject. In the case
of computer science, only universities that published at least 150 articles in the period of
2016--2020 are eligible to be examined. According to~\cite{gras}, \acrshort{gras} measures
the academic performance of universities through the following objective indicators:

\begin{itemize}
    \item \textbf{Research output (Q1)}: the number of articles published by an institution
    in the particular subject in journals with Q1 Journal Impact Factor Quartile during a period.
    \item \textbf{Research influence (CNCI)}: the ratio of citations of articles, published by
    an institution in the respective subject during a period, to the average
    citations of articles in the same field, the same year and same type of journal publication.
    The value of 1 indicates that the citation performance is at world-average level. CNCI less
    than 1 represents the below-average level, while CNCI greater than 1 demonstrates
    above-average citation performance.
    \item \textbf{International influence (IC)}: the percentage of internationally co-authored
    articles conducted by an institution in the particular subject during a period.
    \item \textbf{Research quality (Top)}: the number of academic papers published in top journals
    or top conferences in the respective subject for an institution during a period.
    \item \textbf{International academic awards (Award)}: the number of researchers of an institution
    achieving a prestigious award in the particular subject since 1981.
\end{itemize}

\acrshort{gras} considers a wider set of indicators in calculating the score of
each university. In case of CSRankings, only the
number of academic papers published at top venues is taken into account. This
indicator can be considered as the counterpart of \acrshort{gras}'s
\emph{Research quality (Top)} indicator.

\acrshort{gras} allocates different weights to the indicators for different subjects.
The weights in Computer Science field are listed in \autoref{fig:cs_weight}.
The final scores of institutions are calculated as the sum of the score of each indicator
in a respective subject:

\begin{align*}
    \sum_{i\in \mathbb{L}}\sqrt{P_i}\cdot W_i,
\end{align*}

\begin{table}
    \centering
    \begin{tabular}{|c|c|c|c|c|c|}
        \hline
        Subject & Q1 & CNCI & IC & Top & Award\\
        \hline
        Computer Science & 100 & 100 & 20 & 100 & 100\\
        \hline
    \end{tabular}
    \caption{Indicator weights of Computer Science subject}\label{fig:cs_weight}
\end{table}

where \(W_i\) is the weight of each indicator; \(P_i\) is the percentage of the top scorer
for each indicator, and \(\mathbb{L}\) is the set of indicators \(\{Q1,CNCI,IC,Top,Award\}\). % chktex 21

The webpage of \acrshort{gras} presents the total scores and component scores for each indicator.
They illustrate the academic performance of the department
in general. On the other hand, CSRankings shows total score and individual
score (score of each researcher). Hence, the users can have information about the academic
performance of an individual professor and the department as well.

In terms of conducting surveys, \acrshort{gras} is different from CSRankings
in a number of aspects.
Top journals, top conferences, and top academic awards are selected as the result of the
\gls{aes}. The participants are professors from the top 100 universities, and
many of them are chairs or heads of departments. In contrast to the survey conducted by
CSRankings, ShanghaiRanking \acrshort{aes} publishes the names and affiliated institutions
of all respondents, producing the transparent result of the survey. The survey consists of
two optional questions. The first question asks the respondents to list the top journals and
conferences in their primary subjects. In the second question, participants are required to
propose the most influential and internationally prestigious academic awards in their
primary subjects. According to~\cite{aes}, the selection of a journal, a conference, and an
award is based on the answers of participants along with the following criteria:

\begin{itemize}
    \item \textbf{Top journal}: if it acquires more than one vote in one subject, and it has
    minimum 50\% votes on a particular subject, or it was a top journal in the previous year.
    \item \textbf{Top award}: if it obtains at least one vote in one subject, and it has 50\%
    or more votes on a particular subject, or it was a top award in the previous year.
    \item \textbf{Top conference}: if it is nominated by at least 10 participants, or it was
    a top conference in last year.
\end{itemize}

Considering the requirement of selecting top journals/awards/conferences
raises a question:
``Is it possible that a journal/award/conference will be considered as a top venue,
automatically and indefinitely for the following years,
after it has been selected once?'' For example, a conference was selected in 2017 as it
was voted by 10 survey participants. In 2018, only 5 survey respondents selected it, but
it was still considered a top conference as it was selected in 2017. The same situation
could happen in 2019 or later. The \acrshort{aes} was first published in 2017; therefore,
this problem could be investigated in more detail if the methodology of \acrshort{aes}
2017 can be examined. However, at the time of writing, methodologies
of \acrshort{aes} from 2017 to 2020 are not publicly accessible; thus, it is not possible
to answer this question in this thesis. Nonetheless, it is worth highlighting the potential
issue here to further remark the intricacies of defining objective and unbiased methodologies
for these rankings.

The full list of journals --- conferences, awards, and participants --- is published on this
page: \url{https://www.shanghairanking.com/activities/aes/method/2022}

\subsection{Ideology}
%
\acrshort{gras} aims to provide a reliable university ranking in a wide range
of subjects across Natural Sciences, Engineering, Life Sciences, Medical Sciences,
and Social Sciences. The target audience of \acrshort{gras} are both undergraduate
and graduate students who want to pursue a Bachelor's or Master's program.
In a particular subject field, \acrshort{gras} ranks the department based on its
academic performance in general.

CSRankings differs from \acrshort{gras} in a number of aspects.
CSRankings ranks higher education institutions in a wide range of areas
across Computer Science. The target audience of CSRankings is post-graduate
students who want to pursue a doctoral program. The goal of CSRankings
is to help prospective students to have more information about the research activities
of professors, or research groups, who they are interested in. Thus, it is assumed
that the users of CSRankings have a clear understanding of their research area and
are seeking to identify professors whose research direction aligns with their own.

In conclusion, both rankings aim to provide a reliable ranking system through which
student and university can benefit each other. Students can find their best universities
by researching the ranking, and universities can treat these performance-based rankings
as a motivation to improve themselves and attract more talents.

