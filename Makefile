SHELL := /bin/bash

MAIN=main
TEXSRC=$(wildcard *.tex) $(wildcard tex/*.tex)
BIBSRC=$(wildcard *.bib)
DIR_BUILD=build
OPT= --interaction=nonstopmode

CHKTEX        := $(shell command -v chktex 2> /dev/null)
# ChkTeX manual: https://www.nongnu.org/chktex/ChkTeX.pdf
#
# Default chktex warnings are: `-wall -n19 -n21 -n22 -n30 -n41`
# We elevate them to full errors and enable also
#
# -e19: "You should use ASCII 39 for quote"
# -e41: "You ought to not use primitive TeX in LaTeX code"
#
# For completeness, here is a summary of the warnings that we keep
# disabled as in the default chktex configuration:
#
# -n21: "This command might not be intended"
# -n22: "Comment displayed"
# -n30: "Multiple spaces detected in output"
#
# Finally, we disable:
#
# -n2: "Non-breaking space should have been used"
# -n7: "Accents for i and j (\i, \j)"
# -n8: "Wrong length of dash may have been used"
# -n9: "Brackets/environments order mismatch"
CHKTEX_OPTS   := -eall -n21 -n22 -n30 \
                 -n2 -n7 -n8 -n9 \
				 -n44

## maybe this helps macos ppl
ifeq ($(origin GREP), undefined)
  ifneq (, $(shell which ggrep))
    GREP      := $(shell command -v ggrep 2> /dev/null)
  else
    GREP      := $(shell command -v grep 2> /dev/null)
  endif
endif
GREP_OPTS     := --line-number --color=always


all: $(DIR_BUILD)/$(MAIN).pdf test

$(DIR_BUILD)/$(MAIN).pdf: $(TEXSRC) $(BIBSRC)
	texfot pdflatex -shell-escape $(MAIN).tex
	makeindex -s $(MAIN).ist -t $(MAIN).glg -o $(MAIN).gls $(MAIN).glo
	biber --isbn-normalise $(MAIN)
	texfot pdflatex -shell-escape $(MAIN).tex
	texfot pdflatex -shell-escape $(MAIN).tex
	#latexmk -pdf -pvc -pdflatex="pdflatex $(OPT)" $(MAIN).tex -auxdir=$(DIR_BUILD) -outdir=$(DIR_BUILD)

chktex:
	@printf "\n===== Running ChkTeX on $(MAIN).tex =====\n"
	@printf "### See https://www.nongnu.org/chktex/ChkTeX.pdf\n\n\n"
	$(CHKTEX) $(CHKTEX_OPTS) $(MAIN).tex
.PHONY: chktex

custom_tests: $(MAIN).pdf
	@if $(GREP) $(GREP_OPTS) '\\ref{' $(TEXSRC); then \
		printf "\n===== replace ref with autoref =====\n"; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) '^\\paragraph' $(TEXSRC); then \
		printf "\n===== replace paragraph command with the Paragraph macro =====\n"; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) -P '\t' $(TEXSRC); then \
		printf "\n===== replace tabs with spaces =====\n"; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) '[[:blank:]]$$' $(TEXSRC); then \
		printf "\n===== remove trailing whitespace =====\n"; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) '.\{200\}' $(TEXSRC); then \
		printf "\n===== Long lines detected. Did you hard wrap your text? =====\n"; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) -E '\\(bf|it|tt|rm|sf|sl|em|sc)\>' $(TEXSRC); then \
		printf "\n===== deprecated modal command detected. See https://texfaq.org/FAQ-2letterfontcmd =====\n"; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) -E '\$$\$$' $(TEXSRC); then \
		printf "===== math formula spacing issue: $$$$ detected. See 1.6 at https://www.texlive.info/CTAN/info/l2tabu/english/l2tabuen.pdf =====\n"; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) 'There were undefined citations' $(MAIN).log; then \
		printf "\n===== fix undefined citations =====\n"; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) 'Citation.*on page.*undefined.*' $(MAIN).log; then \
		printf "\n===== fix undefined citations =====\n"; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) 'There were undefined references' $(MAIN).log; then \
		printf "\n===== fix undefined references =====\n"; \
		exit 1; \
	fi
	@if $(GREP) $(GREP_OPTS) 'Warning.*multiply.*defined' $(MAIN).log; then \
		printf "\n===== fix duplicate labels =====\n"; \
		exit 1; \
	fi
.PHONY: custom_tests

test: chktex custom_tests
.PHONY: test

clean:
	rm -f $(DIR_BUILD)/*
	rm -f $(MAIN).{log,aux,bcf,fdb_latexmk,fls,lof,lot,run.xml,synctex.gz,toc,upa,upd,ubp,bbl,blg,glg,glo,gls,ist,out,upb,xmpdata}


.PHONY: clean all 
